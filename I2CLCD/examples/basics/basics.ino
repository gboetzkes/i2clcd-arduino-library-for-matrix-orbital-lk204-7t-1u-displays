/*	Copyright 2014 Martijn Stommels
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or 
 *	any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *     
 *     ---
 *     This example demonstrates the various functions of the 
 *     I2CLCD Arduino library for Matrix Orbital LK204-7T-1U displays.
 */ 

// tell Arduino to use the libary T(you have to include Wire.h first!)
#include <Wire.h>
#include <I2CLCD.h>

// Init a new LCD object at I2C address 0x28
I2CLCD lcd(0x28);


void setup() {
  // join I2C bus
  Wire.begin();
  
  // Tell the display it shouldnt automaticly scroll down
  lcd.autoScroll(false);
  
  // Make the cursor visable so we can see what the display is doing
  lcd.blinkingCursor(true);

}

void loop() {
  // clear the display
  lcd.clear();
  
  // set the cursor to the left corner
  lcd.setCursorPosition(1, 1);
  
  // print some text to the screen (you can only send 32 characters at once)
  lcd.print("Martijn is de beste jongen ");
  lcd.print("van de hele wereld!");
  
  // wait for 2 seconds
  delay(2000);
  lcd.clear();
  

  // set the cursor back to the left corner
  lcd.setCursorPosition(1, 1);
  
  // use println to go to the next row
  lcd.println("Something 1");
  delay(1000);
  lcd.println("Something 2");
  delay(1000);
  lcd.println("Something 3");
  delay(1000);
  lcd.println("Something 4");
  delay(1000);
  lcd.println("Something 5"); // this will be displayed at row 1 because autoscroll is disabled
  
  // wait for 2 seconds
  delay(2000);
  
  // do some funny things with the lights
  for(int i = 0; i < 255; i++)
  {
    lcd.setKeypadBacklight(i);
    lcd.setDisplayBacklight(255 - i);
    delay(20);
  }
  
  // turn the backlight display back on
  lcd.setDisplayBacklight(255);
  
  // set the GPO (general purpose output) pins to turn on the leds
  lcd.setGPO(1, HIGH);
  delay(500);
  lcd.setGPO(2, HIGH);
  delay(500);
  lcd.setGPO(3, HIGH);
  delay(500);
  lcd.setGPO(4, HIGH);
  delay(500);
  lcd.setGPO(5, HIGH);
  delay(500);
  lcd.setGPO(6, HIGH);
  delay(500);
  
  // and off
  lcd.setGPO(1, LOW);
  lcd.setGPO(2, LOW);
  lcd.setGPO(3, LOW);
  lcd.setGPO(4, LOW);
  lcd.setGPO(5, LOW);
  lcd.setGPO(6, LOW);

  // wait for 2 seconds
  delay(2000);
  
}
