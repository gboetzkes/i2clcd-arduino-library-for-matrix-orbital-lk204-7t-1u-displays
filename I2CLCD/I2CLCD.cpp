/*	Copyright 2014 Martijn Stommels
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or 
 *	any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */ 


#include "I2CLCD.h"

I2CLCD::I2CLCD(int address)
{
	_address = address;
}


/// Print text on the display at the current cursor location
void I2CLCD::print(char *text)
{
	Wire.beginTransmission(_address);
	Wire.write(text);
	
	Wire.endTransmission();
}

/// Prints custom character on the display
void I2CLCD::print(int code)
{
	Wire.beginTransmission(_address);
	Wire.write(code);
	
	Wire.endTransmission();
}

/// Prints text and adds a newline at the end
void I2CLCD::println(char *text)
{
	print(text);
	print("\n");
}

/// Removes everyting from the display
void I2CLCD::clear()
{
	Wire.beginTransmission(_address);
	
	// send clear command
	Wire.write(254);
	Wire.write(88);
	
	Wire.endTransmission();
}

/// en/disabel autoscroll
void I2CLCD::autoScroll(bool enabled)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);
	if(enabled)
	{
		Wire.write(81);
	}
	else
	{
		Wire.write(82);
	}
	
	Wire.endTransmission();
}

/// en/disabel auto linewrap
void I2CLCD::autoLineWrap(bool enabled)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);
	if(enabled)
	{
		Wire.write(67);
	}
	else
	{
		Wire.write(68);
	}
	
	Wire.endTransmission();
}

/// Sets the position of the cursor
void I2CLCD::setCursorPosition(int row, int column)
{
	Wire.beginTransmission(_address);
	Wire.write(254);
	Wire.write(71);
	
	// set position
	Wire.write(column);
	Wire.write(row);
	
	Wire.endTransmission();
}

/// en/disable the blinking cursor
void I2CLCD::blinkingCursor(bool enabled)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);
	if(enabled)
	{
		Wire.write(83);
	}
	else
	{
		Wire.write(84);
	}
	
	Wire.endTransmission();
}
/// Sets a GPO pin HIGH or LOW to turn on or of the LEDs
void I2CLCD::setGPO(int GPONumber, bool state)
 {
	 
	Wire.beginTransmission(_address);
	
	Wire.write(254);
	// set state
	if(state)
	{
		Wire.write(87);
	}
	else
	{
		Wire.write(86);
	}
	
	// set GPO number
	Wire.write(GPONumber);
	
	Wire.endTransmission();
 }
/// en/disable the keypad backlight
void I2CLCD::setKeypadBacklight(bool enabled)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);

	if(enabled)
	{
		Wire.write(156);
		Wire.write(255);
	}
	else
	{
		Wire.write(155);
	}
	
	
	Wire.endTransmission();
}

/// set the keypad backlight to a specific brightness level (0 to 255)
void I2CLCD::setKeypadBacklight(int value)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);
	Wire.write(156);
	Wire.write(value);
	
	Wire.endTransmission();
}
/// en/disable the display backlight
void I2CLCD::setDisplayBacklight(bool enabled)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);

	if(enabled)
	{
		Wire.write(66);
	}
	else
	{
		Wire.write(70);
	}
	
	
	Wire.endTransmission();
}

/// set the display backlight to a specific brightness level (0 to 255)
void I2CLCD::setDisplayBacklight(int value)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);

	Wire.write(153);
	Wire.write(value);
	
	
	Wire.endTransmission();
}

/// en/disable automatic keypress transmission (not supported by this libary and should be disabled)
void I2CLCD::autoTransmitKeyPresses(bool enabled)
{
	Wire.beginTransmission(_address);
	
	Wire.write(254);

	if(enabled)
	{
		Wire.write(65);
	}
	else
	{
		Wire.write(79);
	}
	
	Wire.endTransmission();
}

/// Checks if a key was pressed
byte I2CLCD::checkForKeyPress()
{
	Wire.requestFrom(_address, 1);
	while(Wire.available() < 1); // wait for data to come in
	
	return Wire.read();
}


