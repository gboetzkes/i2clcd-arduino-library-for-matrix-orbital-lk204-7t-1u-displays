**	I2CLCD Arduino library for Matrix Orbital LK204-7T-1U displays 	**
**	Written by Martijn Stommels					**

This library lets you use Matrix Orbital LK204-7T-1U displays with Arduino.

To use the library put this folder inside the 'libraries' directory of you Arduino installation. See http://arduino.cc/en/Guide/Libraries for more information.


A few thing should be considered:

You HAVE to include Wire.h in your Arduino project. You also have to call Wire.begin(); before writing anything to the display.
li
See the examples for more information on how to do this.
